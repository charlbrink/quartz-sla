package org.bitbucket.charlbrink.sla.infrastructure

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {SchedulerService.class, SchedulerFactoryBean.class})
@ActiveProfiles("test")
public class SchedulerServiceTest {
    public static final String BUSINESS_REFERENCE_NUMBER = "1230";
    public static final String EXTERNAL_REFERENCE = "1234";

    @Autowired
    private SchedulerService schedulerService;

    @Test
    public void givenJobDataMap_whenSchedulingDuplicate_thenExpectOneExecution() throws SchedulerException {
        Map<String, String> jobDataMap = new HashMap<>();
        jobDataMap.put(CustomerBankStatementsNotReceivedJob.BUSINESS_REFERENCE_NUMBER, BUSINESS_REFERENCE_NUMBER);
        jobDataMap.put(CustomerBankStatementsNotReceivedJob.EXERNAL_REFERENCE_NUMBER, EXTERNAL_REFERENCE);

        schedulerService.scheduleAdHocOnceOffJob(DummyJob.class, "TEST", "TEST", jobDataMap, 2);
        schedulerService.scheduleAdHocOnceOffJob(DummyJob.class, "TEST", "TEST", jobDataMap, 1);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertTrue("DummyJob should have been executed only once", DummyJob.count == 1);
        assertEquals("DummyJob should have been executed with correct jobData", EXTERNAL_REFERENCE, DummyJob.param);

    }

    @Test
    public void givenNoScheduledJob_whenUnschedulingNonExistantJob_thenExpectFailure() throws SchedulerException {
        boolean success = schedulerService.unscheduleJob("TEST1", "TEST");
        assertFalse("Cannot unschedule non-existant job", success);
    }

    @Test
    public void givenScheduledJob_whenUnschedulingExistingJob_thenExpectSuccess() throws SchedulerException {
        schedulerService.scheduleAdHocOnceOffJob(DummyJob.class, "TEST2", "TEST", null, 60);

        boolean success = schedulerService.unscheduleJob("TEST2", "TEST");
        assertTrue("Should be able to unschedule job", success);
    }

    @Slf4j
    public static class DummyJob implements Job {

        private static String param;

        private static int count;

        @Override
        public synchronized void execute(final JobExecutionContext jobExecutionContext) throws JobExecutionException {
            log.info("Executing dummy job");
            param = jobExecutionContext.getJobDetail().getJobDataMap().getString(CustomerBankStatementsNotReceivedJob.EXERNAL_REFERENCE_NUMBER);
            count++;
        }
    }
}
