package org.bitbucket.charlbrink.sla.infrastructure;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.quartz.DailyTimeIntervalScheduleBuilder;
import org.quartz.DailyTimeIntervalTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TimeOfDay;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;

@Slf4j
@Service
public class SchedulerService {
    private SchedulerFactoryBean schedulerFactory;

    public SchedulerService(final SchedulerFactoryBean schedulerFactory) {
        this.schedulerFactory = schedulerFactory;
    }

    public void scheduleAdHocOnceOffJob(final Class<? extends Job> jobClass, final String jobName, final String jobGroup, final Map<String, String> jobData, final int intervalInSeconds)
            throws SchedulerException {
        JobDataMap jobDataMap = new JobDataMap();
        if (jobData != null) {
            jobDataMap.putAll(jobData);
        }
        scheduleJob(jobClass, jobName, jobGroup, jobDataMap, intervalInSeconds, 1);
    }

    private void scheduleJob(final Class<? extends Job> jobClass,
                             final String jobName,
                             final String jobGroup,
                             final JobDataMap jobDataMap,
                             final int intervalInSeconds,
                             final int repeatCount)
            throws SchedulerException {

        Scheduler scheduler = schedulerFactory.getScheduler();

        JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
        TriggerKey triggerKey = TriggerKey.triggerKey(jobName, jobGroup);

        if (!scheduler.checkExists(jobKey)) {
            register(jobClass, jobKey, jobDataMap, true);
        }

        JobDetail jobDetail = scheduler.getJobDetail(jobKey);
        DailyTimeIntervalScheduleBuilder dailyTimeIntervalScheduleBuilder = DailyTimeIntervalScheduleBuilder.dailyTimeIntervalSchedule().startingDailyAt(TimeOfDay.hourAndMinuteAndSecondFromDate(Date
                .from(LocalDateTime.now().plus(intervalInSeconds, ChronoUnit.SECONDS).atZone(ZoneId.systemDefault()).toInstant()))).withRepeatCount(repeatCount);
        DailyTimeIntervalTrigger onceOffJobTrigger = TriggerBuilder.newTrigger().withIdentity(triggerKey).forJob(jobDetail).withSchedule(dailyTimeIntervalScheduleBuilder).build();

        if (!scheduler.checkExists(triggerKey)) {
            Date scheduledAt = scheduler.scheduleJob(onceOffJobTrigger);
            log.debug("Job scheduled with jobData [{}] to run at [{}]", MapUtils.toProperties(jobDataMap), scheduledAt);
        } else {
            Date rescheduledAt = scheduler.rescheduleJob(triggerKey, onceOffJobTrigger);
            log.debug("Job rescheduled with jobData [{}] to run at [{}]", MapUtils.toProperties(jobDataMap), rescheduledAt);
        }
    }

    private void register(final Class<? extends Job> jobClass, final JobKey jobKey, final JobDataMap jobDataMap, final boolean replace)
            throws SchedulerException {
        Scheduler scheduler = schedulerFactory.getScheduler();
        JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobKey).usingJobData(jobDataMap).storeDurably().build();
        scheduler.addJob(jobDetail, replace);
    }

    public boolean unscheduleJob(final String jobName, final String jobGroup) throws SchedulerException {
        boolean wasUnscheduled = false;
        Scheduler scheduler = schedulerFactory.getScheduler();

        TriggerKey triggerKey = TriggerKey.triggerKey(jobName, jobGroup);

        if (scheduler.checkExists(triggerKey)) {
            wasUnscheduled = scheduler.unscheduleJob(triggerKey);
            log.debug("Job unscheduled with triggerKey [{} {}] was successful [{}]", triggerKey.getName(), triggerKey.getGroup(), wasUnscheduled);
        } else {
            log.warn("Job not found to unschedule with triggerKey [{} {}]", triggerKey.getName(), triggerKey.getGroup());
        }
        return wasUnscheduled;
    }

}
